package com.seasuncc.oauth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan(basePackages = "com.seasuncc.oauth.mapper")
@SpringBootApplication
public class SeasunccOauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(SeasunccOauthApplication.class, args);
    }

}
