package com.seasuncc.oauth.service.impl;

import com.seasuncc.oauth.domain.TbUser;
import com.seasuncc.oauth.mapper.TbUserMapper;
import com.seasuncc.oauth.service.TbUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TbUserServiceImpl implements TbUserService {

	@Autowired
	private TbUserMapper tbUserMapper;

	@Override
	public TbUser getByUsername(String username) {
		return tbUserMapper.findByUserName(username);
	}

}
