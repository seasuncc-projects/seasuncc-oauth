package com.seasuncc.oauth.service;

import com.seasuncc.oauth.domain.TbPermission;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/24
 */
public interface TbPermissionService {

    List<TbPermission> selectByUserId(Long userId);
}
