package com.seasuncc.oauth.service;

import com.seasuncc.oauth.domain.TbUser;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/24
 */
public interface TbUserService {

    TbUser getByUsername(String username);
}
