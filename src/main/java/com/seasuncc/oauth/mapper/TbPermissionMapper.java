package com.seasuncc.oauth.mapper;

import com.seasuncc.oauth.domain.TbPermission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/24
 */
@Repository
public interface TbPermissionMapper {

    List<TbPermission> selectByUserId(@Param("userId") Long userId);
}
