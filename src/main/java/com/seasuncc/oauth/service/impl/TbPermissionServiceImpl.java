package com.seasuncc.oauth.service.impl;

import com.seasuncc.oauth.domain.TbPermission;
import com.seasuncc.oauth.mapper.TbPermissionMapper;
import com.seasuncc.oauth.service.TbPermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TbPermissionServiceImpl implements TbPermissionService {

	@Autowired
	private TbPermissionMapper tbPermissionMapper;

	@Override
	public List<TbPermission> selectByUserId(Long userId) {
		return tbPermissionMapper.selectByUserId(userId);
	}

}
