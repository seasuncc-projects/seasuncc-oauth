package com.seasuncc.oauth.mapper;

import com.seasuncc.oauth.domain.TbUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * TODO: 类中文描述
 *
 * @author shidoudou
 * @date 2020/9/24
 */
@Repository
public interface TbUserMapper {
    TbUser findByUserName(@Param("username") String username);
}
